#!/bin/perl
use strict;
use warnings;
use Cache::File;
use Cache::RemovalStrategy::LRU;
use Config::YAML;
use Cwd qw( abs_path );
use Data::Dumper;
use Desktop::Notify;
use File::Basename;
use File::Path qw( make_path );
use Time::HiRes qw( sleep gettimeofday tv_interval );
use Time::Piece;

# Flush the buffer after each write rather than waiting until there are enough bytes in the buffer to send it
$|=1;

# Work variables
my ($c,$conf_update,%cache);

# Files and directories
my $dirname = dirname(abs_path($0));
my $scriptname = ($0 =~ /(\/)?(\w+)(\.pl)?$/)?$2:$0;
my $cache_dir = $dirname.'/.cache';
my $logfile = $dirname."/".$scriptname.".log";
my $configfile = $dirname."/".$scriptname.".yml";

# Main
# ------------------------------------------------------------------------------
# Initialize environment
init_env();

# Print bar content
print_coolbar();

# Schedule tasks
cron_jobs();
    
# Listen for STDIN events
listen_stdin();

# Sub-functions
# ------------------------------------------------------------------------------
sub init_env {
    mylog("init_env - initializing environment");
    # If configuration file exists, load it else create it
    if (-e $configfile) {
        $c = Config::YAML->new( config => $configfile );
    }
    else {
        open my $fc, ">", $configfile;
        close $fc;
        $c = Config::YAML->new(
            config => $configfile,
            output => $configfile,
            log => { info => 0, debug => 0, stdin => 0 },
            notification => { timeout => 5000 },
            update => { install => 0 },
            day => [ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ],
            month => [ "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december" ],
            icon => [ "🔥", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" ]
        );
        $c->write;
    }
    $conf_update = (stat($configfile))[9];
    # If debug mode, print interesting infos
    if ($c->{log}->{debug}) {
        mylog("\$c = ".Dumper(\$c));
        mylog("\$cache_dir = ".$cache_dir);
        mylog("\$dirname = ".$dirname);
        mylog("\$scriptname = ".$scriptname);
        mylog("\$configfile = ".$configfile);
    }
    # Create cache directory if not exists
    if ($c->{log}->{info}) { mylog("init_env - initialize cache and populate it"); }
    unless (-e $cache_dir) { make_path($cache_dir) or die+("init_env - failed to create ".$cache_dir.": ".$!); }
    # Initialize cache with tie interface
    tie %cache, 'Cache::File', { cache_root => $cache_dir.'/coolbar.cache',
                                 lock_level => Cache::File::LOCK_LOCAL(),
                                 default_expires => '86400 sec' };
    # Initialize default values
    ## Set default icon and notification body for update block
    $cache{'update_icon'} = $c->{icon}[2];
    $cache{'update_body'} = "No check performed yet... Please be patient and come back later...";
    ## Set icons that are never updated (no dedicated sub-functions for these blocks)
    $cache{'time'} = $c->{icon}[4];
    $cache{'bluetooth'} = $c->{icon}[15];
    $cache{'keyring'} = $c->{icon}[16];
}

sub print_coolbar {
    # Fork process
    my $pid = fork();
    if (!defined($pid)) {
        mylog("print_coolbar - can not fork a child: ".$!);
        return;
    }
    elsif ($pid == 0) {
        # Print header so that sway-bar knows we want to use JSON
        print("{ \"version\":1, \"click_events\":true }\n");
        print("[\n");
        print("[]\n");
        # Print status line
        mylog("print_coolbar - printing status");
        while (1) {
            #mylog("print_coolbar - printing status in forked process");
            # Run tasks
            get_audio();
            # Send JSON blocks
            print(",[\n");
            print("{\"name\":\"id_network\",\"full_text\":\"".$cache{'network'}."\"},\n");
            print("{\"name\":\"id_pubip\",\"full_text\":\"".$cache{'pubip'}."\"},\n");
            print("{\"name\":\"id_uname\",\"full_text\":\"".$cache{'uname'}."\"},\n");
            print("{\"name\":\"id_uptime\",\"full_text\":\"".$cache{'uptime'}."\"},\n");
            print("{\"name\":\"id_battery\",\"full_text\":\"".$cache{'battery'}."\"},\n");
            print("{\"name\":\"id_audio\",\"full_text\":\"".$cache{'audio'}."\"},\n");
            print("{\"name\":\"id_date\",\"full_text\":\"".$cache{'date'}."\"},\n");
            print("{\"name\":\"id_time\",\"full_text\":\"".$cache{'time'}." ".mytime('hms')."\"},\n");
            print("{\"name\":\"id_bluetooth\",\"full_text\":\"".$cache{'bluetooth'}."\"},\n");
            print("{\"name\":\"id_keyring\",\"full_text\":\"".$cache{'keyring'}."\"},\n");
            print("{\"name\":\"id_update\",\"full_text\":\"".$cache{'update_icon'}."\"}\n");
            print("]\n");
            sleep(0.4);
        }
        exit;
    }
}

sub listen_stdin {
    while (my $line = <>) {
        chomp($line);
        if ($c->{log}->{stdin}) { unless($line eq "[") { mylog("stdin events - ".$line); } }
        # Network block
        if ($line =~ /"id_network"/) { }
    
        # Network block
        if ($line =~ /"id_pubip"/) {
            if ($line =~ /"button": [13]/) { my @out = `alacritty --class="floating_term" --option "window.dimensions={lines: 21, columns: 72}" -e /bin/bash -c 'dig myip.opendns.com \@resolver1.opendns.com && read -n 1 -r -s'`; }
        }
    
        # Kernel block
        if ($line =~ /"id_uname"/) {
            if ($line =~ /"button": [13]/) { my @out = `alacritty --class="floating_term" --option "window.dimensions={lines: 24, columns: 77}" -e /bin/bash -c 'neofetch && read -n 1 -r -s'`; }
        }
    
        # Uptime block
        if ($line =~ /"id_uptime"/) {
            if ($line =~ /"button": [13]/) { my @out = `alacritty --class="floating_term" --option "window.dimensions={lines: 40, columns: 110}" -e /bin/bash -c 'bpytop'`; }
        }
    
        # Battery block
        if ($line =~ /"id_battery"/) {
            if ($line =~ /"button": 1/) { my @out = `alacritty --class="floating_term" -o "window.dimensions={lines: 6, columns: 43}" -o "background_opacity=1.0" -e /bin/bash -c 'upower -i /org/freedesktop/UPower/devices/battery_BAT0 | /sbin/grep -E "state|energy:|voltage|percentage" && read -n 1 -r -s'`; }
            if ($line =~ /"button": 3/) { my @out = `alacritty --class="floating_term" -o "window.dimensions={lines: 24, columns: 70}" -e /bin/bash -c 'upower -i /org/freedesktop/UPower/devices/battery_BAT0 && read -n 1 -r -s'`; }
        }
    
        # Audio block
        if ($line =~ /"id_audio"/) {
            if ($line =~ /"button": [12]/) { my @out = `pamixer --toggle-mute`; }
            if ($line =~ /"button": 3/) { my @out = `alacritty --class="floating_term" --option "window.dimensions={lines: 40, columns: 125}" -e /bin/bash -c 'alsamixer -c 1'`; }
            if ($line =~ /"button": 4/) { my @out = `pamixer -i 1`; }
            if ($line =~ /"button": 5/) { my @out = `pamixer -d 1`; }
        }
    
        # Calendar block
        if ($line =~ /"id_date"/) {
            if ($line =~ /"button": [13]/) {
                my @out = `alacritty --class="floating_term"  --option "window.dimensions={lines: 35, columns: 78}" -e /bin/bash -c 'cal -y -w && read -n 1 -r -s'`;
            }
        }
    
        # Clock block
        if ($line =~ /"id_time"/) {
            if ($line =~ /"button": [13]/) {
                my @out = `alacritty --class="floating_term" -o "window.dimensions={lines: 40, columns: 125}" -o "colors.primary={background: '#000000'}" -e /bin/bash -c 'curl wttr.in && read -n 1 -r -s'`;
            }
        }
    
        # Bluetooth block
        if ($line =~ /"id_bluetooth", "button": 1/) { my @out = `blueman-manager`; }
    
        # Keyring block
        if ($line =~ /"id_keyring", "button": 1/) { my @out = `seahorse`; }
    
        # Update block
        if ($line =~ /"id_update", "button": 1/) {
            notify($cache{'update_icon'}." Update status\n",$cache{'update_body'});
        }
    }
}

sub notify {
    my ($summary,$body) = @_;
    my $notify = Desktop::Notify->new;
    my $notification = $notify->create(summary => $summary, body => $body, timeout => $c->{notification}->{timeout});
    $notification->show();
}

sub mytime {
    my ($style) = @_;
    my $t = localtime;
    unless($style) { return $t; }
    ($style =~ /^hms$/) && return($t->hms);
    ($style =~ /^dmy$/) && return($t->dmy("/"));
    ($style =~ /^dmyw$/) && return($t->dmy("/")." [".$t->week."]");
    ($style =~ /^Ddmyw$/) && return($t->day(@{$c->{day}})." ".$t->dmy("/")." [".$t->week."]");
    ($style =~ /^DdMy$/) && return($t->day(@{$c->{day}})." ".(($t->mday==1)?"1er":$t->mday)." ".$t->month(@{$c->{month}})." ".$t->year);
    ($style =~ /^DdMyw$/) && return($t->day(@{$c->{day}})." ".(($t->mday==1)?"1er":$t->mday)." ".$t->month(@{$c->{month}})." ".$t->year." [".$t->week."]");
    return $t;
}

sub mylog {
    my ($msg)=@_;
    open(my $log,">>",$logfile) or die("mylog - can't open log file: ".$!);
    print $log ("[".mytime('dmy')." ".mytime('hms')."] ".$scriptname." - ".$msg."\n");
    close $log;
}

sub cron_jobs {
    if ($c->{log}->{info}) { mylog("cron_jobs - launch task scheduler"); }
    # Start important tasks
    get_network();
    get_uname();
    get_uptime();
    get_battery();
    get_date();
    #update_repository();
    check_update();
    get_pubip();
    # Launch scheduler in its own forked process
    my $pid = fork();
    if (!defined($pid)) {
        mylog("cron_jobs - can not fork a child: ".$!);
        return;
    }
    elsif ($pid == 0) {
        if ($c->{log}->{info}) { mylog("cron_jobs - start internal crontab"); }
        # Check each seconds if task(s) must be launched
        while (1) {
            my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
            #my($seconds, $microseconds) = gettimeofday;
            #mylog("cron_jobs - seconds = ".$seconds." and microseconds = ".$microseconds);
            # Each seconds
            check_conf();
            # Each minutes starting at h:m:00s
            if ($sec == 0) {
                get_uptime();
                # Each hour starting at 00:00:00
                if ($min == 0) {
                    get_pubip();
                    # Each day at 00:00:00
                    if ($hour == 0) { 
                        get_date();
                    }
                }
                # Each 30min starting at h:00:00
                if ($min % 30 == 0) {
                    #update_repository();
                    check_update();
                }
            }
            # Each 10s starting at h:m:00
            if ($sec % 10 == 0 ) { 
                get_network();
                get_battery();
            }
            # Temporization
            sleep(1);
        }
        exit;
    }
}

sub check_conf {
    if ($c->{log}->{info}) { mylog("check_conf - check if configuration file has been modified"); }
    my $last_conf_update = (stat($configfile))[9];
    if ($last_conf_update > $conf_update) {
        if ($c = Config::YAML->new(config => $configfile)) {
            if ($c->{log}->{debug}) { mylog("check_conf - new configuration file loaded"); }
            $conf_update = $last_conf_update;
            # Update data for forked process
            $cache{'timer'} = $c->get_timer;
            # Set icons that are never updated (no dedicated sub-functions for these blocks)
            $cache{'time'} = $c->{icon}[4];
            $cache{'bluetooth'} = $c->{icon}[15];
            $cache{'keyring'} = $c->{icon}[16];
        }
    }
}

sub update_repository {
    if ($c->{log}->{info}) { mylog("update_repository - update arch local index"); }
    my $pid = fork();
    if (!defined($pid)) {
        mylog("update_repository - can not fork a child: ".$!);
        exit;
    }
    elsif ($pid == 0) {
        my @updaterep = `sudo /usr/bin/pacman -Sy`;
        if ($c->{log}->{debug}) { foreach my $line (@updaterep) { chomp($line) && mylog("update_repository - ".$line); } }
        exit;
    }
}

sub check_update {
    if ($c->{log}->{info}) { mylog("check_update - check updates availability"); }
    #update_repository();
    my @updaterep = `sudo /usr/bin/pacman -Sy`;
    # Check updates availability
    my @updlist = `/usr/bin/pacman -Qu`;
    if ($c->{log}->{debug}) { foreach my $line (@updlist) { chomp($line) && mylog("check_update - ".$line); } }
    my $n = scalar(@updlist);
    if ($n) {
        if ($c->{log}->{debug}) { mylog("check_update - ".$n." update(s) available"); }
        if ($c->{update}->{install}) {
            # Silently install updates
            my @install=`sudo /usr/bin/pacman -Syu --noconfirm`;
            if ($c->{log}->{debug}) { foreach my $line (@install) { chomp($line) && mylog("check_update - ".$line); } }
            # Check if install succeeds
            @updlist = `/usr/bin/pacman -Qu`;
            if ($c->{log}->{debug}) { foreach my $line (@updlist) { chomp($line) && mylog("check_update - ".$line); } }
            my $u = scalar(@updlist);
            if ($u) { 
                if ($c->{log}->{debug}) { mylog("check_update - ".$u." update(s) available after auto install ?!? Something surely got wrong..."); }
                $cache{'update_icon'} = $c->{icon}[0];
                $cache{'update_body'} = $u." update(s) still available after auto install ?!? Something surely got wrong... Please upgrade manually to check what happened.\n";
            }
            else {
                if ($c->{log}->{debug}) { mylog("check_update - $n update(s) successfully installed"); }
                $cache{'update_icon'} = $c->{icon}[1];
                $cache{'update_body'} = "Success ! $n update(s) successfully installed\n";

            }
        }
        else {
            $cache{'update_icon'} = $c->{icon}[0];
            $cache{'update_body'} = $n." update(s) available. Please upgrade your system.\n";
            foreach my $item (@updlist) {
                chomp($item);
                if ($c->{log}->{debug}) { mylog("check_update - ::. ".$item); }
                $cache{'update_body'} .= "::. ".$item."\n";
            }
        }
    }
    else {
        if ($c->{log}->{debug}) { mylog("check_update - no update available, system is up-to-date"); }
        $cache{'update_icon'} = $c->{icon}[1];
        $cache{'update_body'} = "Good ! System is up-to-date.\n";
    }
    # Check if a new kernel is ready to be loaded
    my $uname = `uname -r`;
    chomp($uname);
    my $release;
    my @out = `/usr/bin/pacman -Qs '^linux\$'`;
    foreach my $line (@out) {
        chomp($line);
        if ($line =~ /^.+linux\s+(.+)$/) { $release = $1; }
    }
    my $r=$release;
    $r =~ s/-/\./g;
    my $u=$uname;
    $u =~ s/-/\./g;
    unless ($r eq $u) {
        if ($c->{log}->{debug}) { mylog("check_update - new kernel release installed, system needs reboot (".$uname." -> ".$release.")"); }
        $cache{'update_icon'} = $c->{icon}[0];
        $cache{'update_body'} .= "\nImportant: new kernel has been installed that requires a system reboot to be loaded\n";
    }
}

sub get_uname {
    if ($c->{log}->{info}) { mylog("get_uname - verify kernel release"); }
    my $uname = `uname -r`;
    chomp($uname);
    $cache{'uname'} = $c->{icon}[6]." ".$uname;
}

sub get_date {
    if ($c->{log}->{info}) { mylog("get_date - check if today is a new day"); }
    $cache{'date'} = $c->{icon}[3]." ".mytime('Ddmyw');
}

#sub get_uptime_old {
#    if ($c->{log}->{info}) { mylog("get_uptime - update pretty uptime value"); }
#    my $s=`uptime -p`;
#    chomp $s;
#    $s =~ s/^up\s+//;
#    $s =~ s/(\d+)\s+d\w+,?/$1j/;
#    $s =~ s/\s*(\d+)\s+h\w+,?/$1h/;
#    $s =~ s/\s*(\d+)\s+m\w+,?/$1m/;
#    $cache{'uptime'} = $c->{icon}[5]." ".$s;
#}

sub get_uptime {
    if ($c->{log}->{info}) { mylog("get_uptime - update pretty uptime value"); }
    my ($w,$d,$h,$m);
    my $s=`uptime -p`;
    chomp $s;
    $s =~ s/^up\s+//;
    $s =~ s/(\d+)\s+d\w+,?/$1j/;
    $s =~ s/\s*(\d+)\s+h\w+,?/$1h/;
    $s =~ s/\s*(\d{2})\s+m\w+,?/$1m/;
    $s =~ s/(h)\s*(\d)\s+m\w+,?/${1}0$2m/;
    $cache{'uptime'} = $c->{icon}[5]." ".$s;
}

#sub get_pubip_old {
#    if ($c->{log}->{info}) { mylog("get_pubip - get public ip"); }
#    my $pid = fork();
#    if (!defined($pid)) {
#        mylog("get_pubip - can not fork a child: ".$!);
#        return;
#    }
#    elsif ($pid == 0) {
#        my $ip=`dig +short myip.opendns.com \@resolver1.opendns.com`;
#        chomp($ip);
#        if ($c->{log}->{debug}) { mylog("get_pubip - dig result: ".$ip); }
#        $cache{'pubip'} = ($ip !~ /^\d+\.\d+\.\d+\.\d+$/)?"":$c->{icon}[7]." ".$ip;
#        exit;
#    }
#}

sub get_pubip {
    if ($c->{log}->{info}) { mylog("get_pubip - get public ip"); }
    my $ip=`dig +short myip.opendns.com \@resolver1.opendns.com`;
    chomp($ip);
    if ($c->{log}->{debug}) { mylog("get_pubip - dig result: ".$ip); }
    $cache{'pubip'} = ($ip !~ /^\d+\.\d+\.\d+\.\d+$/)?"":$c->{icon}[7]." ".$ip;
}

sub get_network {
    if ($c->{log}->{info}) { mylog("get_network - get network status"); }
    my @out = `ip -o -4 a`;
    my $network = "";
    foreach my $line (@out) {
        if ($line =~ /.+lo.+127\.0\.0\.1.+/) { next; }
        if ($line =~ /^[^\s]+\s+([^\s]+)\s+[^\s]+\s+([^\s]+)\s+/) {
            my($dev,$inet) = ($1,$2);
            if ($c->{log}->{debug}) { mylog("get_network - \$dev=".$dev." and \$inet=".$inet); }
            $network .= (($dev=~/wl/)?$c->{icon}[9]:(($dev=~/tun/)?$c->{icon}[10]:$c->{icon}[8]))." ".$dev."@".$inet." ";
        }
    }
    $network =~ s/^(.+)\s+$/$1/;
    $cache{'network'} = $network;
}

sub get_audio {
    if ($c->{log}->{info}) { mylog("get_audio - get audio settings"); }
    my ($icon,$volume);
    my $mute = `pamixer --get-mute`;
    chomp $mute;
    if ($mute =~ /true/) { $icon = $c->{icon}[14]; }
    else {
        $volume = `pamixer --get-volume`;
        chomp $volume;
        if ($volume >= 60) { $icon = $c->{icon}[13]; }
        elsif ($volume >= 20) { $icon = $c->{icon}[12]; }
        elsif ($volume >= 1) { $icon = $c->{icon}[11]; }
        else { $icon = $c->{icon}[14]; }
    }
    $cache{'audio'} = $icon.($volume?" ".$volume."%":"");
}

sub get_battery {
    if ($c->{log}->{info}) { mylog("get_battery - get battery info"); }
    my $icon;
    my $status = `cat /sys/class/power_supply/BAT0/status`;
    chomp $status;
    my $capacity = `cat /sys/class/power_supply/BAT0/capacity`;
    chomp $capacity;
    if ($c->{log}->{debug}) { mylog("get_battery - \$status=".$status.", \$capacity=".$capacity); }
    # Correction if capacity appears superior to 100%
    if ($capacity > 100) { $capacity = 100; }
    if ($status =~ /^Charging$/) { $icon = $c->{icon}[22]; }
    else {
        if ($capacity >= 90) { $icon = $c->{icon}[21]; }
        elsif ($capacity >= 60) { $icon = $c->{icon}[20]; }
        elsif ($capacity >= 40) { $icon = $c->{icon}[19]; }
        elsif ($capacity >= 10) { $icon = $c->{icon}[18]; }
        else { $icon = $c->{icon}[17]; }
    }
    $cache{'battery'} = $icon." ".$capacity."%";
    $cache{'battery_status'} = $status;
}
