# Installation

## Perl script way

1. Copy the file `coolbar.pl` where you want, in the example below i copy it to ~/.config/coolbar/

2. Modify your sway config file at bar section (or create a new one), like example below :

```
bar {
    id coolbar
    position top 
    height 25
    status_edge_padding 0
    separator_symbol ''
    font pango:DejaVu Sans Mono, Font Awesome 5 Free Solid 9
    status_command perl ~/.config/coolbar/coolbar.pl
    colors {
        statusline #ffffff
        background #69696940
        active_workspace #696969 #696969 #ffffff
        inactive_workspace #32323200 #32323200 #5c5c5c
        focused_workspace #a9a9a9 #a9a9a9 #ffffff
    }   
    workspace_buttons yes 
    strip_workspace_numbers true
}
```


## Perl compilation way

1. Compile the code with PAR::Packer or other

```
$ pp -o coolbar coolbar.pl
```

2. Modify your sway config file at bar section (or create a new one), like example below :

```
bar {
    id coolbar
    position top 
    height 25
    status_edge_padding 0
    separator_symbol ''
    font pango:DejaVu Sans Mono, Font Awesome 5 Free Solid 9
    status_command ~/.config/coolbar/coolbar
    colors {
        statusline #ffffff
        background #69696940
        active_workspace #696969 #696969 #ffffff
        inactive_workspace #32323200 #32323200 #5c5c5c
        focused_workspace #a9a9a9 #a9a9a9 #ffffff
    }   
    workspace_buttons yes 
    strip_workspace_numbers true
}
```

# Configuration

Some settings can be modified in coolbar.yml :

```
# Timer display (top right of coolbar)
timer: 0
# Logging
log:
  info: 0
  debug: 0
  stdin: 0
# Notification settings
notification:
  timeout: 5000
# Updates management
update:
  install: 1
# Week days
day:
  - dimanche
  - lundi
  - mardi
  - mercredi
  - jeudi
  - vendredi
  - samedi
# Months
month:
  - janvier
  - février
  - mars
  - avril
  - mai
  - juin
  - juillet
  - août
  - septembre
  - octobre
  - novembre
  - décembre
```

# Yet to do

- Actions on blocks mouse clicks to complete
- Temperature block
- CPUs block
- Trap stop/kill signal to end properly
- i18n
- ...Many other things :)

All suggestions welcome !!
